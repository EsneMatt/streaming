CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE "user" IF NOT EXISTS
(
    "id" UUID NOT NULL,
    "first_name" VARCHAR(255) NOT NULL,
    "last_name" VARCHAR(255) NOT NULL,
    "birthdate" DATE NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "active" BOOLEAN NOT NULL,

    PRIMARY KEY ("id"),
    UNIQUE ("email")
);

CREATE TABLE "subscription" IF NOT EXISTS
(
    "id" UUID NOT NULL,
    "startDate" DATE NOT NULL,
    "endDate" DATE NOT NULL,

    PRIMARY KEY ("id")
);

CREATE TABLE "offer" IF NOT EXISTS
(
    "id" UUID NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "available" BOOLEAN NOT NULL,
    "durationMonth" INT NOT NULL,
    "price" FLOAT NOT NULL,

    PRIMARY KEY ("id"),
    UNIQUE ("name")
);

CREATE TABLE "paypal" IF NOT EXISTS
(
    "id" UUID NOT NULL,
    "mail" VARCHAR(255) NOT NULL,

    PRIMARY KEY ("id")
    UNIQUE ("mail")
)

CREATE TABLE "creditCard" IF NOT EXISTS
(
    "id" UUID NOT NULL,
    "number" INT NOT NULL,
    "expirationDate" DATE NOT NULL

    PRIMARY KEY ("id")
    UNIQUE("mail")
)