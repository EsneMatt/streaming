package org.stcharles.streaming.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.stcharles.streaming.domain.model.Subscription;

import java.util.UUID;

public interface SubscriptionRepository extends JpaRepository<Subscription, UUID> {
}
