package org.stcharles.streaming.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.stcharles.streaming.domain.model.Payment;

import java.util.UUID;

public interface PaymentRepository extends JpaRepository<Payment, UUID> {
}
