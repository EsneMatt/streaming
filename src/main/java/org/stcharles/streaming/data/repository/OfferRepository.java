package org.stcharles.streaming.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.stcharles.streaming.domain.model.Offer;

import java.util.UUID;

public interface OfferRepository extends JpaRepository<Offer, UUID> {
}
