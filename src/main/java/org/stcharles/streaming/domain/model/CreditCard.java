package org.stcharles.streaming.domain.model;

import jakarta.persistence.*;

import java.util.Date;
import java.util.UUID;

@Table(name="creditCard")
public class CreditCard extends Payment {
    @Column(name="id")
    private UUID id;
    @Column(name="number")
    private int number;
    @Column(name="expirationDate")
    private Date expiration_date;

    public CreditCard(UUID id, int number, Date expiration_date) {
        super();
        this.id = id;
        this.number = number;
        this.expiration_date = expiration_date;
    }

    public UUID get_id() {
        return id;
    }

    public void set_id(UUID id) {
        this.id = id;
    }

    public int get_number() {
        return number;
    }

    public void set_number(int number) {
        this.number = number;
    }

    public Date get_expiration_date() {
        return expiration_date;
    }

    public void set_expiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }


    @Override
    public void debit() {
        // TODO fill in
    }

    @Override
    public void credit() {
        // TODO fill in
    }
}