package org.stcharles.streaming.domain.model;

public abstract class Payment {
    public abstract void debit();
    public abstract void credit();
}