package org.stcharles.streaming.domain.model;

import jakarta.persistence.*;

import java.util.UUID;

@Table(name ="paypal")
public class PayPal extends Payment {
    @Column(name="id")
    private UUID id;
    @Column(name="mail")
    private String mail;

    public PayPal(UUID id, String mail) {
        super();
        this.id = id;
        this.mail = mail;
    }

    public UUID get_id() {
        return id;
    }

    public void set_id(UUID id) {
        this.id = id;
    }

    public String get_mail() {
        return mail;
    }

    public void set_mail(String mail) {
        this.mail = mail;
    }

    @Override
    public void debit() {
        // TODO fill in
    }

    @Override
    public void credit() {
        // TODO fill in
    }
}