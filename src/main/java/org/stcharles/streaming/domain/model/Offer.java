package org.stcharles.streaming.domain.model;

import jakarta.persistence.*;

import java.util.UUID;
@Table(name = "offer")
public class Offer {
    @Id
    private UUID id;
    @Column(name = "name")
    private String text;
    @Column(name = "available")
    private boolean available;
    @Column(name= "durationMonth")
    private int durationMonth;
    @Column(name="price")
    private float price;

}