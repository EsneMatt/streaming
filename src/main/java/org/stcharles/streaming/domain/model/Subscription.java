package org.stcharles.streaming.domain.model;

import jakarta.persistence.*;

import java.util.Date;
import java.util.UUID;
@Table(name = "subscription")
public class Subscription {
    @Id
    private UUID id;
    @Column(name = "startDate")
    private Date startDate;
    @Column(name = "endDate")
    private Date endDate;

}
