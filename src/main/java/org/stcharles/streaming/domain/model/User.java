package org.stcharles.streaming.domain.model;

import jakarta.persistence.*;

import java.util.UUID;
import java.util.Date;

@Entity
@Table(name="user")
public class User {
    @Id
    private UUID ID;
    @Column(name="first_name")
    private String first_name;
    @Column(name="last_name")
    private String last_name;
    @Column(name="birthdate")
    private Date birthdate;
    @Column(name="mail")
    private String mail;
    @Column(name="active")
    private boolean active;

    public User() {}
    public User(UUID ID, String first_name, String last_name, Date birthdate, String mail, boolean active) {
        this.ID = ID;
        this.first_name = first_name;
        this.last_name = last_name;
        this.birthdate = birthdate;
        this.mail = mail;
        this.active = active;
    }

    public UUID get_id() {
        return ID;
    }

    public String get_first_name() {
        return first_name;
    }

    public String get_last_name() {
        return last_name;
    }

    public Date get_birthdate() {
        return birthdate;
    }

    public String get_mail() {
        return mail;
    }

    public boolean isActive() {
        return active;
    }

    public void set_ID(UUID ID) {
        this.ID = ID;
    }

    public void set_first_name(String first_name) {
        this.first_name = first_name;
    }

    public void set_last_name(String last_name) {
        this.last_name = last_name;
    }

    public void set_birthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public void set_mail(String mail) {
        this.mail = mail;
    }

    public void activate() {
        active = true;
    }

    public void deactivate() {
        active = false;
    }
}